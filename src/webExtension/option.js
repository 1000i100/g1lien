browser.tabs.query({active: true, currentWindow: true}, function(tabs) {
    browser.tabs.executeScript(tabs[0].id, {file: "content_script.js"});
    var fileChooser = document.createElement("input");
    fileChooser.type = 'file';
    fileChooser.accept = '.json';
    document.querySelector("form").appendChild(fileChooser);
});