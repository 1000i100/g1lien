
// Ce script s'éxecute sur une page web ouverte par l'utilisateur en revanche
// Si on est sur un menu firefox ou sur une page de démarrage non-web ce script ne s'éxécute pas
alert("contentscript");

fileChooser.addEventListener('changes', function (evt) {
    var confFile = evt.target.files[0];
    const text = file2txt(confFile);
    appendTxt(text);
});

async function file2txt(file){
    return new Promise((resolve,reject) => {
        const fileReader = new FileReader();
        fileReader.addEventListener('load',() => resolve(fileReader.result));
        fileReader.readAsText(file);
    });
}

function saveOption (text) {
    browser.storage.sync.set({"conf":text});
}

function appendTxt(txt, nodeToAppend) {
    const node = document.createElement("p");
    node.innerHTML = txt;
    nodeToAppend.appendChild(node);
}