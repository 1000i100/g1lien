export function g1UriToCesiumHash(g1uri){
  const parts = extractG1LinkContent(g1uri).split(":");
  const action = parts.shift();
  try{
    return supportedActions[action](...parts);
  } catch (e){
    if(!parts.length) return supportedActions['wallet'](action);
    e.message = `unsupportedAction: ${action}`;
    throw e;
  }
}
function extractG1LinkContent(url){
  if (url.slice(0,4) === "web+") url = url.slice(4);
  if (url.slice(3,5) === "//") url = url.slice(5);
  if (url.slice(1,3) === "1:") url = url.slice(3);
  return url;
}
const profil = (wallet)=>`#/app/wot/${wallet}/`;
const payConfirm = (amount,_to,pubKey,comment)=>`#/app/transfer/${pubKey}?amount=${amount}${comment?'&comment='+comment:''}`;
const supportedActions = {
  "pubkey":profil,
  "uid":profil,
  "wallet":profil,
  "pay":payConfirm,
  "tip":payConfirm,
  "balance":(wallet)=>`#/app/wot/tx/${wallet}/` // actuellement, ne marche qu'avec une pubkey
}
