import test from 'ava';
import * as app from './g1UriToCesium.mjs';

const testCases = {
  "g1://pubkey:D6Pm9VsPTLqYMwUtcXxXBqdGP9pMXMkd76C1xZXsF3yg": "#/app/wot/D6Pm9VsPTLqYMwUtcXxXBqdGP9pMXMkd76C1xZXsF3yg/",
  "G1:uid:1000i100": "#/app/wot/1000i100/",
  "Ǧ1://wallet:1000i100": "#/app/wot/1000i100/",
  "g1:wallet:AZERTYUIOP1234567890azertyuiop": "#/app/wot/AZERTYUIOP1234567890azertyuiop/",
  "G1://pay:50:to:AZERTYUIOP1234567890azertyuiop:bienvenue l'autre": "#/app/transfer/AZERTYUIOP1234567890azertyuiop?amount=50&comment=bienvenue l'autre",
  "ǧ1:tip:4,99:to:AZERTYUIOP1234567890azertyuiop": "#/app/transfer/AZERTYUIOP1234567890azertyuiop?amount=4,99",
  "web+g1:uid:1000i100": "#/app/wot/1000i100/",
  "balance:1000i100": "#/app/wot/tx/1000i100/",
  "g1:D6Pm9VsP":"#/app/wot/D6Pm9VsP/",
//    "Ǧ1:isMember:Hiroty": "localhost:3000/isMember/Hiroty/",
//    "ǧ1://isSentry:1000i100": "localhost:3000/isSentry/1000i100/",
//    "g1:isRefer:Yannick": "localhost:3000/isSentry/Yannick/",
//  "ns:1000i100": "2sZF6j2PkxBDNAqUde7Dgo5x3crkerZpQ4rBqqJGn8QT",
//  "isBalance:50:on:1000i100": "Il y a bien 50 Junes sur le porte feuille: 1000i100",
};
Object.keys(testCases).forEach((k)=>test(k,t=>t.is(app.g1UriToCesiumHash(k),testCases[k])));

test("unsupported syntax thow error",t=>{
  const err = t.throws(()=>app.g1UriToCesiumHash("g1:plop:plip"));
  t.is(err.message,"unsupportedAction: plop");
});

//XXX: g1://pay:50:to:1000i100 -> https://cesium.g1.1000i100.fr/api/#/v1/payment/2sZF6j2PkxBDNAqUde7Dgo5x3crkerZpQ4rBqqJGn8QT?amount=50

