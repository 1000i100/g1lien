
// La fonction init() permet d'initialiser la configuration du nom de domaine que l'on souhaite utiliser
// Le format doit être en JSON
// Exemple -> {"isRefer":"duniter.g1.1000i100.fr"}
// La configuration fera pointé la fonction "isRefer" vers le domaine de "duniter.g1.1000i100.fr"

let globalConf;
export function init(
  json={  //configuration par défaut
    "pubkey":"cesium.g1.1000i100.fr",
    "uid":"cesium.g1.1000i100.fr",
    "ns":"cesium.g1.1000i100.fr",
    "wallet":"cesium.g1.1000i100.fr",
    "pay":"cesium.g1.1000i100.fr",
    "tip":"cesium.g1.1000i100.fr",
    "balance":"cesium.g1.1000i100.fr",
    "isBalance":"cesium.g1.1000i100.fr",
    "isMember":"localhost:3000",
    "isSentry":"localhost:3000",
    "isRefer":"localhost:3000",
    "cesium":"cesium.g1.1000i100.fr" //choix pour la conversion des liens
  })
{ globalConf = json; }
//module.exports.init = init; //indispensable pour passer les tests
export function choix_action(url) { //g1:pubkey:D6Pm9VsPTLqYMwUtcXxXBqdGP9pMXMkd76C1xZXsF3yg

  if (url.slice(0,4) === "web+")
    url = url.slice(5);
  else
    url = url.slice(1);
  url = "g"+url;

  if (url.search("//") == -1)
    url = url.replace("g1:","");
  else
    url = url.replace("g1://","");

  const argument = url.split(":");

  switch (argument[0]) {
    case "pubkey":
      return pubkey(argument[1]);
      break;

    case "uid":
      return uid(argument[1]);
      break;

    case "ns":
      return name_service(argument[1]);
      break;

    case "wallet":
      return wallet(argument[1]);
      break;

    case "pay":
      return pay(argument[1],argument[3],argument[4]);
      break;

    case "tip":
      return tip(argument[1],argument[3],argument[4]);
      break;

    case "balance":
      return balance(argument[1]);
      break;

    case "isBalance":
      return isBalance(argument[1],argument[3]);
      break;

    case "isMember":
      return isMember(argument[1]);
      break;

    case "isSentry":
      return isSentry(argument[1]);
      break;

    case "isRefer": // est un alias de isSentry
      return isSentry(argument[1]);
      break;

    default:
      //console.log("L'argument 1 du lien G1 ne correspond pas aux actions disponible");
      throw "Format non reconnu";
      break;
  }
}
// module.exports.choix_action = choix_action; //indispensable pour passer les tests
