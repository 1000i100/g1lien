import * as g1Cesium from './g1UriToCesium.mjs';

const startG1LienMatcher = '(?:web\\+|)[GgǦǧ]1:(?://|)';
const endG1LienMatcher = ' ';

let instancePrefix;

export function init(instance){
  if(instance.indexOf('://') === -1) instancePrefix = `https://${instance}/`;
  else instancePrefix = instance;
}

let isConverterRunning=false;
export function live(htmlConversionFunc=convertHtmlOnce){
  document.g1HtmlLinkifier_liveTriggersRemover = [];
  const removerList = document.g1HtmlLinkifier_liveTriggersRemover;
  const launcher = ()=> {
    if(isConverterRunning) return;
    isConverterRunning=true;
    htmlConversionFunc(document.body);
    isConverterRunning=false;
  }
  const timeoutTrigger1 = setTimeout(launcher,1000);
  removerList.push(()=>clearTimeout(timeoutTrigger1));
  const timeoutTrigger2 = setTimeout(launcher,10000);
  removerList.push(()=>clearTimeout(timeoutTrigger2));
  const intervalTrigger = setInterval(launcher,60*1000);
  removerList.push(()=>clearInterval(intervalTrigger));
  document.body.addEventListener("click",launcher);
  removerList.push(()=>document.body.removeEventListener("click",launcher));
}
export function liveStop(){
  document.g1HtmlLinkifier_liveTriggersRemover.forEach(remover=>remover());
}
export function convertHtmlOnce(htmlNode) {
    if(htmlNode.childNodes.length)
        for(let i = 0;i<htmlNode.childNodes.length;i++)
            convertHtmlOnce(htmlNode.childNodes[i]);
    //console.log(htmlNode.nodeName)
    if(htmlNode.nodeName === 'A'){ // si c'est une balise html <a>
        if(new RegExp(startG1LienMatcher).test(htmlNode.href))
            try{
                htmlNode.href = instancePrefix + g1Cesium.g1UriToCesiumHash(htmlNode.href); //TODO: use g1Router
            } catch(e){
                console.log(htmlNode.href,e);
            }
    }

    if(new RegExp(startG1LienMatcher).test(htmlNode.nodeValue)) // si le texte contient un g1lien
        textNodeConvertion(htmlNode);
}

function textNodeConvertion(textNode) {

    if(textNode.parentNode.nodeName === 'A') return;

    const text = textNode.nodeValue;

    const textParts = [];
    let textBuffer = text;
    while(1){
        const g1StartPos = textBuffer.search(new RegExp(startG1LienMatcher));
        if(g1StartPos===-1) break;
        textParts.push(textBuffer.substr(0,g1StartPos));
        textBuffer= textBuffer.substr(g1StartPos);

        const g1EndPos = textBuffer.search(new RegExp(endG1LienMatcher));
        if(g1EndPos===-1) break;
        textParts.push(textBuffer.substr(0,g1EndPos));
        textBuffer= textBuffer.substr(g1EndPos);
    }
    textParts.push(textBuffer);
    textParts.forEach((txt,i) => {
        if (!txt) return;
        if(i%2===0) addBefore(textNode,document.createTextNode(txt));
        else addBefore(textNode,g1ToHTMLLink(txt));
    });
    textNode.parentNode.removeChild(textNode);
}

function addBefore(refNode,newNode){
    refNode.parentNode.insertBefore(newNode,refNode);
}

export function g1ToHTMLLink(g1lien) {
    try{
        const http = instancePrefix + g1Cesium.g1UriToCesiumHash(g1lien); //TODO: use g1Router
        const lien = document.createElement('a');
        lien.appendChild(document.createTextNode(g1lien));
        lien.href = http;
        return lien;
    }catch(e) {
        return document.createTextNode(g1lien);
    }
}
