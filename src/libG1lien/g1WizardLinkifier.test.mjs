import test from 'ava';
import jsdom from 'jsdom';
global["document"] = (new jsdom.JSDOM('')).window.document;
import * as app from './g1WizardLinkifier.mjs';

function replaceInHtml(originalHtml){
  document.body.innerHTML = originalHtml;
  app.convertHtmlOnce(document.body);
  return document.body.innerHTML;
}
const gText = 'web+G1:uid:Hiroty';
const gLink = "https://demo.cesium.app/#/app/wot/Hiroty/";
const gNode = `<a href="${gLink}">web+G1:uid:Hiroty</a>`;

test("reproduit le même comportement que g1HtmlLinkifier avant de le surchager", t => {
  t.is(replaceInHtml(`<h1>${gText}</h1>`) , `<h1>${gNode}</h1>`);
});
