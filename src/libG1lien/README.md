# lib Ǧ1link / Ǧ1lien
[![npm release](https://img.shields.io/npm/v/g1link.svg)](https://www.npmjs.com/package/g1link)
[![pipeline status](https://framagit.org/1000i100/g1lien/badges/master/pipeline.svg)](https://framagit.org/1000i100/g1lien/-/graphs/master/charts)
[![coverage report](https://framagit.org/1000i100/g1lien/badges/master/coverage.svg)](https://1000i100.frama.io/g1lien/coverage/)

For **Ǧ1lien spec** and other Ǧ1lien stuff, see [Ǧ1lien full project](https://framagit.org/1000i100/g1lien).

- **g1HtmlLinkifier** parse your webpage to convert g1links to usable link (ask for base url if not configured with a default one).

  [Test it on the demo page](https://1000i100.frama.io/g1lien/)

- **g1UriToCesium** low level lib : convert Ǧ1lien syntaxe to Cesium Syntaxe.

## How to use

To handle Ǧ1lien in your web site :
```
<script src="g1WizardLinkifier.min.js"></script>
<script>
    g1WizardLinkifier.live();
</script>
```
To handle Ǧ1lien in your tools, check unit test or export function, name should be explicit.
You can use them as esm modern javascript module (.esm.js or .mjs), as browser bare metal lib,
as amd in require.js, as cjs in node... (.js or .min.js for all these cases)
Just include/require the good file for your ecosystem.

See [the demo page](https://1000i100.frama.io/g1lien/) for exemple.

