import * as g1HtmlLinkifier from './g1HtmlLinkifier.mjs';

const activeConfig = { // default
  install:"https://framagit.org/1000i100/g1lien#%C7%A71lien",
  learn:"https://monnaie-libre.fr/",
  fallback:"https://demo.cesium.app/"
};

export function init(config){
  Object.keys(config).forEach(key=>{
    activeConfig[key] = config[key].indexOf('://') === -1 ? `https://${instance}/` : config[key];
  });
}
export function live(){
  g1HtmlLinkifier.live(convertHtmlOnce);
}
export function liveStop(){
  g1HtmlLinkifier.liveStop();
}
export function removeToolTips(){
  document.querySelectorAll(`a[href^="${activeConfig.fallback}"]`).forEach(linkNode=>{
    linkNode.rmG1WizardToolTip();
  });
}
export function convertHtmlOnce(htmlNode) {
  g1HtmlLinkifier.init(activeConfig.fallback);
  addToolTipStyle(document);
  g1HtmlLinkifier.convertHtmlOnce(htmlNode);
  htmlNode.querySelectorAll(`a[href^="${activeConfig.fallback}"]`).forEach(linkNode=>{
    if(typeof linkNode.rmG1WizardToolTip !== "undefined") return ;
    const ttFn = (e)=>{
      if(!e.target.href) return e.preventDefault();
      if(e.target !== linkNode) return;
      e.preventDefault();
      addToolTip(linkNode,getTranslatedToolTipMessage(linkNode.href));
    };
    linkNode.addEventListener("click",ttFn);
    linkNode.rmG1WizardToolTip = ()=>linkNode.removeEventListener("click",ttFn);
  });
}
function getTranslatedToolTipMessage(fallback){
  const supportedLangTTMessage = {
    "fr":`
      <strong>Ce lien fait référence à la <a href="${activeConfig.learn}">monnaie libre</a> Ǧ1.</strong>
      <p>
        Pour l'activer : <a href="${activeConfig.install}">installer<br/> un outil compatible</a>.
      </p>

      <p><small style="float:right"><a href="${fallback}">Consulter le lien</a> en mode démo.</small></p>
      `,
    "en":`
      <strong>This link refer to <a href="${activeConfig.learn}">libre currency</a> Ǧ1.</strong>
      <p>
        To enable it : <a href="${activeConfig.install}">install a compatible tool</a>.
      </p>

      <p><small style="float:right"><a href="${fallback}">Browse link</a> in demo mode.</small></p>
      `
  }
  let lang = getUserLang();
  if(!supportedLangTTMessage[lang]) lang = "en";
  return supportedLangTTMessage[lang];
}
function getUserLang() {
  return navigator.language.substring(0, 2).toLowerCase();
}
function addToolTipStyle(domDocument){
  if(domDocument.getElementById('g1TTStyle')) return ;
  const style = document.createElement("style");
  style.id = 'g1TTStyle';
  const content = `
  <style>
  .g1ToolTip {
    position: absolute;
    left: 0;
    top: 110%;
    padding: 10px;
    width: 235px;
    z-index: 5;
    background-color: rgba(98%,98%,98%,.95);
    border: solid 3px rgba(98%,90%,30%,.95);
    color: #222;
    text-decoration: none;
    cursor: default;
  }
  .g1ToolTip h5{margin-top: 0;}
  .g1TTClose {
    float: right;
    padding: 10px;
    margin: -10px;
    font-size: 200%;
    line-height: 30px;
    width: 30px;
    text-align: center;
    cursor: pointer;
  }
  .g1TTClose:hover {
    transform: scale(1.2);
  }
  .g1TTHostLink {
    position: relative;
  }
  </style>
  `;
  style.innerHTML = content.replace(/<[\/]?style>/g,"");
  domDocument.head.appendChild(style);
}
function addToolTip(linkNode,htmlContent){
  if(linkNode.querySelector(".g1ToolTip")) return ;
  const toolTip = document.createElement("div");
  toolTip.classList.add("g1ToolTip");

  // X en haut à droite
  const close = document.createElement("a");
  close.innerHTML = "⨯";
  close.classList.add("g1TTClose");
  close.addEventListener("click",e=> toolTip.remove() || e.preventDefault() );
  toolTip.appendChild(close);
  // texte pédagogique
  const content = document.createElement("div");
  content.innerHTML = htmlContent;
  toolTip.appendChild(content);

  linkNode.classList.add("g1TTHostLink");
  linkNode.appendChild(toolTip);
}
