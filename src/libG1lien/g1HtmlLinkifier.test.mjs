import test from 'ava';
import jsdom from 'jsdom';
global["document"] = (new jsdom.JSDOM('')).window.document;
import * as app from './g1HtmlLinkifier.mjs';


function xtest (txt){console.log("Disabled test : ",txt);}

app.init("demo.cesium.app");

test(`encapsule une chaine de caractère dans une balise < a href="" / >`, t => {

  const resultNode = app.g1ToHTMLLink('G1://pubkey:4Fmb3EsGbbwjmZ7zRySyGSdFroSCpQ9TY46ZHweiVf85');
    t.is(resultNode.href , 'https://demo.cesium.app/#/app/wot/4Fmb3EsGbbwjmZ7zRySyGSdFroSCpQ9TY46ZHweiVf85/');
    t.is(resultNode.innerHTML , 'G1://pubkey:4Fmb3EsGbbwjmZ7zRySyGSdFroSCpQ9TY46ZHweiVf85');
});

function replaceInHtml(originalHtml){
  document.body.innerHTML = originalHtml;
  app.convertHtmlOnce(document.body);
  return document.body.innerHTML;
}
const gText = 'web+G1:uid:Hiroty';
const gLink = "https://demo.cesium.app/#/app/wot/Hiroty/";
const gNode = `<a href="${gLink}">web+G1:uid:Hiroty</a>`;

test("remplace un g1lien dans une balise simple", t => {
    t.is(replaceInHtml(`<h1>${gText}</h1>`) , `<h1>${gNode}</h1>`);
});
test("remplace un g1lien dans une page HTML simple", t => {
    t.is(replaceInHtml(`<h1>Ǧsper ${gText} bien</h1>`) , `<h1>Ǧsper ${gNode} bien</h1>`);
});
test("remplace plusieurs g1liens dans une page HTML simple", t => {
    t.is(replaceInHtml(`<h1>Ǧsper ${gText} mais aussi ${gText}</h1>`) , `<h1>Ǧsper ${gNode} mais aussi ${gNode}</h1>`);
});
test("remplace un g1lien dans une page HTML avec balise fille", t => {
    t.is(replaceInHtml(`<h1><b>Ǧsper</b> ${gText}</h1>`) , `<h1><b>Ǧsper</b> ${gNode}</h1>`);
});
test("remplace des g1liens imbriqué dans une page HTML avec balise fille", t => {
    t.is(replaceInHtml(`<h1>avant ${gText} <b>ici ${gText} et</b> la ${gText} après</h1>`) , `<h1>avant ${gNode} <b>ici ${gNode} et</b> la ${gNode} après</h1>`);
});
test("converti un g1lien dans un href sans changer le libélé", t => {
    t.is(replaceInHtml(`<h1><a href="${gText}">Par ici les dons</a></h1>`) , `<h1><a href="${gLink}">Par ici les dons</a></h1>`);
});
test("laisse un g1lien incorrect en href sans planter", t => {
    t.is(replaceInHtml(`<h1>${gText}<a href="g1:invalid:link">Par ici les dons</a>${gText}</h1>`) , `<h1>${gNode}<a href="g1:invalid:link">Par ici les dons</a>${gNode}</h1>`);
});
test("converti les g1liens correct même après un g1lien incorrect", t => {
    t.is(replaceInHtml(`<p> ${gText} <b>g1:\\pay:50:to:Hiroty</b> ${gText} </p>`) , `<p> ${gNode} <b>g1:\\pay:50:to:Hiroty</b> ${gNode} </p>`);
});


xtest("effectue la correspondance des noms de domaine dans un href", t => {

    //TODO: coder dans l'API une fonction qui actualise la liste des noeuds Cesium

    t.is(replaceInHtml(`<h1><a href="https://g1.le-sou.org/#/app/wot/Hiroty/">Par ici les dons</a></h1>`) , `<h1><a href="https://demo.cesium.app/#/app/wot/Hiroty/">Par ici les dons</a></h1>`);
});
